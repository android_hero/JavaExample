package com.cherry.example.reflect;

public class Person {

    public String name;

    private long id;

    private Person() {
    }

    public Person(String name) {
        this.name = name;
    }

    public static boolean run() {
        System.out.println("runing...");
        return false;
    }

    private void sleep(){
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                '}';
    }
}
