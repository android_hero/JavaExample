package com.cherry.example.reflect;

public class Teacher extends Person{

    public int age;
    private int weight;

    public Teacher(String name) {
        super(name);
    }

    @Override
    public String toString() {
        return "Person{" +
                "weight='" + weight + '\'' +
                '}';
    }
}
