package com.cherry.example.patterns.observer;

//预定天气预报的接口。
public interface BookWeather {
	
	public void notifyWeather(String weather);
	
}
