package com.cherry.example.patterns.observer;

import java.util.ArrayList;
import java.util.Random;

import javax.security.auth.x500.X500Principal;

/*
观察者设计模式：  观察者设计模式解决的是当一个对象发生了指定动作的时候要通知另外一个对象作出相应的处理。

需求： 编写一个气象站，气象站不断的在更新天气，当气象站更新天气的时候人就要做出相应的处理。

问题一：  天气只是更新了一次，人做出两次响应。 

解决方案：

问题2： 目前关注天气的人只有一个，其实在现实生活中是有很多人关注天气的。

问题3： 在现实生活中，不单止员工群体需要关注天气，其他的群体也需要关注天气。


程序高度耦合..一个类依赖另外一个类。 我们在做程序开发的时候尽量不要过分依赖其他的类。


观察者设计模式的步骤：
	如果A 类发生了指定动作的时候要通知B 类有相应的行为，那么 设计完A类的时候就应该定义个接口，然后在A类中维护接口的实现类对象，
	当A类发生动作的时候就调用接口实现类对象的方法即可。



*/
public class WeatherStation {
	
	//定义一个数组存储天气的数据
	String[] weathers = {"暴雨","下雪","台风","雾霾","冰雹"};
	
	//当前天气
	String weather;
	
	Random random = new Random();
	
	//员工对象
	ArrayList<BookWeather> list = new ArrayList<BookWeather>();
	
	//添加员工
	public void addListner(BookWeather e){
		list.add(e);
	}
	
	
	//开始工作
	public void startWork(){
		new Thread(){
			
			@Override
			public void run() {
				while(true){
					updateWeather();
					//调用人的方法
					for(BookWeather e : list){
						e.notifyWeather(weather);
					}
					//1~1.5秒更新一次天气
					int s = random.nextInt(501)+1000; 
					try {
						Thread.sleep(s);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
			
		}.start();	
		
	}
	
	
	//更新天气
	public void updateWeather(){
		
		//随机产生 一个索引值
		int index = random.nextInt(weathers.length);
		weather = weathers[index];
		System.out.println("当前天气是："+weather);
		System.out.println('1'+"23");
	}
	
	
	
	
}
