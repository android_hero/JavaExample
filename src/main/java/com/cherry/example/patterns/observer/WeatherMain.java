package com.cherry.example.patterns.observer;

import java.util.Random;

public class WeatherMain {
	
	public static void main(String[] args) throws Exception {
		Emp e1 = new Emp("刘攀");
		Emp e2 = new Emp("德伦");
		
		Student s1 = new Student("徐刚");
		Student s2 = new Student("子键");
		
		
		WeatherStation station = new WeatherStation();
		//添加收听天气预报的人
		station.addListner(e1);
		station.addListner(e2);
		station.addListner(s1);
		station.addListner(s2);
		
		
		station.startWork();
		
		
	}

}
