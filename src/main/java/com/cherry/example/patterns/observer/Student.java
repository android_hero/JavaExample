package com.cherry.example.patterns.observer;

public class Student implements BookWeather {
	
	String name;

	public Student(String name) {
		this.name = name;
	}
	
	//根据天气变化做出相应的处理
	public void notifyWeather(String weather){
		if("暴雨".equals(weather)){
			System.out.println(name+"在被窝里面睡觉 ..");
		}else if("下雪".equals(weather)){
			System.out.println(name+"在被子里面玩游戏吃泡面..");
		}else if("台风".equals(weather)){
			System.out.println(name+"站在阳台上装逼..");
		}else if("雾霾".equals(weather)){
			System.out.println(name+"去教室睡觉 ..");
		}else if("冰雹".equals(weather)){
			System.out.println(name+"在宿舍睡觉..");
		}
	}
	

}
