package com.cherry.example.patterns.observer;

public class Emp implements BookWeather {
	
	String name;

	public Emp(String name) {
		this.name = name;
	}
	
	
	public Emp() {
		super();
	}


	//"暴雨","下雪","台风","雾霾","冰雹"
	//根据天气变化做出相应的处理
	public void notifyWeather(String weather){
		if("暴雨".equals(weather)){
			System.out.println(name+"撑着雨伞来上班 ..");
		}else if("下雪".equals(weather)){
			System.out.println(name+"牵妹子的手过来上班..");
		}else if("台风".equals(weather)){
			System.out.println(name+"拖着一块大石头过来上班..");
		}else if("雾霾".equals(weather)){
			System.out.println(name+"戴着防毒面具过来上班..");
		}else if("冰雹".equals(weather)){
			System.out.println(name+"戴着钢盔过来上班..");
		}
			
		
	}
	

}
