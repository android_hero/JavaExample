package com.cherry.example.thread.demo;
/*
yield() 线程让步(线程会释放当前的cpu的执行权，重新参与竞争cpu的执行权)

*/
class BankThread extends Thread{
	
	static int money = 5000;
	
	public BankThread(String name){
		super(name);
	}
	
	@Override
	public  void run() {
		while(true){
			
			synchronized ("锁") {
				if(money>0){
					System.out.println(Thread.currentThread().getName()+"取出了100块，剩余："+ (money-100)+"元");
					money-=100;
				}else{
					System.out.println("已经取光了..");
					break;
				}
			}
			Thread.currentThread().yield(); //线程让步
		}
	}	
}
 
public class Demo8 {
	
	public static void main(String[] args) {
		//创建两个线程对象
		BankThread  thread1 = new BankThread("老公");
		BankThread  thread2 = new BankThread("老婆");
		//启动线程
		thread1.start();
		thread2.start();
		
		
		
	}
}
