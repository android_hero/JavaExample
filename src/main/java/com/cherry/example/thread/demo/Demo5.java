package com.cherry.example.thread.demo;

/*
线程的停止： 

停止线程要注意的事项：
	1. 我们停止一个线程往往都是通过一个变量去控制的。
	2. 如果需要停止一个处于临时阻塞状态下的线程，那么变量必须要配合interrupt方法一起使用才行。
	

*/
public class Demo5 extends Thread {

    boolean flag = true;

    public Demo5(String name) {
        super(name);
    }

    @Override
    public synchronized void run() {
        int i = 0;
        System.out.println(this);
        while (flag) {
            try {
                this.wait();  //狗娃
            } catch (InterruptedException e) {

            }
            System.out.println("i = " + i);
            i++;
        }
    }


    public static void main(String[] args) {
        //创建自定义线程
        Demo5 d = new Demo5("狗娃");
        //开启线程
        d.start();

        for (int i = 0; i < 100; i++) {
            System.out.println(Thread.currentThread().getName() + " : " + i);
            //当主线程的i等于80的时候，停止自定义线程。
            if (i == 80) {
                //停止狗娃线程
                d.flag = false;        //interrupt(); 方法没法停止一个线程。
                //强制清除线程的临时阻塞状态。
                d.interrupt();

            }
        }
    }
}
