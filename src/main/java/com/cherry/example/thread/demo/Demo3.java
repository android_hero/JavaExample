package com.cherry.example.thread.demo;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/*
练习： 使用线程创建方式二实现售票的例子。 ( 三个窗口)


同步代码块的劣势： 获取或与释放锁的过程是隐式的。 


jdk1.5提出了一个新的同步机制：  Lock. 

	
synchronized 与Lock的区别：
	1.  synchronized获取或与释放锁的过程是隐式的， 而Lock释放锁与获取锁是显式的。 

Lock常用的方法：
	lock()  获取锁
	unLock()  释放锁。
	

*/


class SaleTicket implements Runnable{
	
	 int num  = 50 ;	//票数
	 
	 //创建一个锁对象
	 Lock lock = new ReentrantLock();
	 
	 
	 
	@Override
	public void run() {
		while(true){
			lock.lock();// 获取锁
				if(num>0){
					System.out.println(Thread.currentThread().getName()+"售出了第"+num+"号票");
					num--;
				}else{
					System.out.println(Thread.currentThread().getName()+"票已售罄");
					lock.unlock();  //释放锁
					break;
				}
			lock.unlock(); //释放锁
		}
	}	
}


public class Demo3 {
	
	public static void main(String[] args) {
		//创建Runnable实现类的对象
		SaleTicket saleTicket = new SaleTicket();
		//创建Thread类的对象
		Thread t1 = new Thread(saleTicket,"窗口1");
		Thread t2 = new Thread(saleTicket,"窗口2");
		Thread t3 = new Thread(saleTicket,"窗口3");
		//启动线程
		t1.start();
		t2.start();
		t3.start();
		
		
		
		
	}
}
