package com.cherry.example.thread.demo;
/*
join() 方法： 线程加入

需求： 模拟小时候打酱油。 
*/

class Mother extends Thread{
	
	@Override
	public void run() {
		System.out.println("妈妈洗菜");
		System.out.println("妈妈切菜");
		System.out.println("妈妈发现没有酱油");
		//通知儿子打酱油
		Son son = new Son();
		son.start();
		//加入线程
		try {
			son.join(); //执行join语句的线程会让步给新加入的线程先完成任务,然后自己再继续完成自己的任务。 
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		System.out.println("妈妈继续炒菜");
		System.out.println("全家一块吃饭");
	}
} 


//儿子类
class Son extends Thread{
	
	@Override
	public void run() {
		System.out.println("儿子下楼..");
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println("儿子一直往前走...");
		System.out.println("儿子打到酱油...");
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		System.out.println("儿子上楼把酱油给老妈...");
	}
	
}



public class Demo7 {
	
	public static void main(String[] args) {
		Mother mother = new Mother();
		mother.start();
		
		
	}
}
