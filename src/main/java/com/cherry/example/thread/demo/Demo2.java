package com.cherry.example.thread.demo;
/*
 
方式一： 通过继承Thread类。
		1. 自定一个类继承Thread类。
		2. 子类重写Thread类的run方法，在run内部编写自定义线程的任务代码。
		3. 创建Thread子类的对象，然后调用start方法启动线程。
	 
线程的创建方式二：
	1.  声明一个类实现了Runnable接口。
	2. 实现了Runnable接口的run方法。编写自定义线程的任务代码。
	3. 创建Runnable实现类的对象。
	4. 创建Thread类的对象，然后把Runnable实现类的对象作为参数传入。 
	5. 调用thread类的start方法开启线程，。

推荐使用： 推荐使用方式二(Runnable实现方式)。  原因： 因为java是单继承的,多实现。 
 

疑问一： 请问Runnable实现类对象是线程对象吗？	
		不是线程对象， 只有Thread与Thread的子类才是属于线程对象。Runanble实现类对象只不过是一个普通的对象而已。 
	

疑问二： 为什么把要Runnable实现类对象作为参数传递给Thread呢？其作用是什么？ 
		
		可以让线程把Runnable实现类对象的run方法作为任务代码执行。 


练习： 使用线程创建方式二实现售票的例子。 ( 三个窗口)

*/
public class Demo2 implements Runnable{

	@Override
	public void run() {
		//编写线程的任务代码
		for(int i = 0; i <100 ; i++){
			System.out.println(Thread.currentThread().getName()+":" + i);
		/*	System.out.println("this:"+ this);
			System.out.println(Thread.currentThread());*/
		}
	}
	

	public static void main(String[] args) {
		//创建Runnable实现类的对象
		Demo2 d = new Demo2();
		//创建Thread对象
		Thread thread = new Thread(d, "狗娃"); //参数一： runnable实现类的对象。  参数二： 指定线程的名字。
		//开启线程
		thread.start();
		
		for(int i = 0; i <100 ; i++){
			System.out.println("主线程："+ i);
		}		
	}
}




/*
 使用该 new Thread(d, "狗娃")方法创建线程对象的时候，
 内部是适应了taget变量记录了runnable实现类对象。
 

Thread类的run方法源码： 
	
	 public void run() {
        if (target != null) {
            target.run();  // 把Runnable实现类的run方法作为了线程的任务代码执行了。
        } 
    }


*/







