package com.cherry.example.thread.demo;
/*
后台线程（守护线程）： 一个进程中只剩下了后台线程，那么该后台线程也必须死亡。

需求： 模拟QQ下载更新包。

操作后台线程的方法： 
	isDaemon() 判断是否为守护线程。
	
*/

class UpdateThread extends Thread{
	
	@Override
	public void run() {
		for (int i = 1; i <=100; i++) {
			System.out.println("已经下载到"+i+"%");
			if(i==100){
				System.out.println("正在安装中...");
			}
			try {
				Thread.sleep(1);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}	
}
 
public class Demo6 {
	
	public static void main(String[] args) {
		
		UpdateThread update = new UpdateThread();
		//设置update线程为守护线程
		update.setDaemon(true);
		//System.out.println("update你是守护线程吗？"+ update.isDaemon());
		update.start();
		
		for (int i = 1; i <=100; i++) {
			System.out.println(Thread.currentThread().getName()+" ： "+i);
		}
		
	}
}
