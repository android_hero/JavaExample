package com.cherry.example.thread;

import java.util.Timer;
import java.util.TimerTask;

/*
定时器: Timer


执行定时任务的方法：
	 schedule(TimerTask task, long delay)   一次  安排在指定延迟后执行指定的任务。 
  		//参数一： 指定定时任务。
  		 * 参数二： 指定间隔的毫秒数之后，执行定时任务。（一次）
  		
  		
  	schedule(TimerTask task, long delay, long period)   多次
		参数一： 指定定时任务。
  		  参数二： 指定间隔的毫秒数之后，执行定时任务。
  		  参数三： 每隔指定的毫秒数再执行n次。

定时任务： TimerTask 
*/
public class TimerDemo {
	
	public static void main(String[] args) {
		//创建一个定时器
		Timer timer = new Timer();
		//执行定时任务
		timer.schedule(new MyTask(timer), 1000,1000);
		
	}

	//自定义一个类继承TimerTask
	static class MyTask extends TimerTask{

		Timer timer;

		public MyTask(Timer timer) {
			this.timer = timer;
		}

		@Override
		public void run() {
			//定时任务要执行的代码..
			System.out.println("炸.....");
//		timer.cancel();
		}
	}
}
