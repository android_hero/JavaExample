package com.cherry.example.network.tcp;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;

//服务端
public class Demo1Server {
	
	public static void main(String[] args) throws IOException {
		//第一步:建立tcp连接, 并且要监听一个端口 
		ServerSocket serverSocket = new  ServerSocket(9090);
		//第二步： 接收客户端的请求连接
		Socket socket = serverSocket.accept();  //accept 方法也是一个阻塞型的方法，如果没有客户端与其连接，会一直等待。
		//第三步： 获取输入流对象，读取数据
		byte[] buf = new byte[1024];
		int length = 0; 
		InputStream  inputStream = socket.getInputStream();
		 length = inputStream.read(buf);
		System.out.println("服务端读取到的数据："+ new  String(buf,0,length));
		
		
		//给客户端回送一条数据
		String data = "客户端你辛苦啦！";
		//获取一个输出流对象
		OutputStream outputStream = socket.getOutputStream();
		outputStream.write(data.getBytes());
		
		
		
		//第四步： 关闭资源
		serverSocket.close();
	}

}
