package com.cherry.example.network.tcp;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;

/*

udp协议的特点：
	1. 通讯不要建立连接。
	2. 发送的数据都必须要封装到数据包中，每个数据包的大小不能超过64kb。
	3. 因为是不需要建立连接，所以是一个不可靠的协议（会出现数据包丢失）
	4. 因为不需要建立连接，所以效率高。
	5. udp是不分客户端与服务端的，只分发送端与接收端。


tcp协议：
	1. tcp的服务一旦启动就必须马上建立连接。
	2. tcp协议发送数据是基于IO流进行数据的传输的，没有大小限制。
	3. 通讯之前使用了三次握手机制，可靠的协议。 (保证数据传输的完整性)。
	4. 因为需要建立连接，所以效率低。
	5. tcp是分客户端与服务端的。
	
比如： 打电话，QQ聊天、

tcp协议涉及到的类：

	Socket   客户端类
	ServerSocket 服务端类。
	
*/

//tcp客户端
public class Demo1Client {
	
	public static void main(String[] args) throws IOException {
		// 第一步： 建立tcp的服务。
		Socket socket = new  Socket(InetAddress.getLocalHost(), 9090);
		//第二步： 获取socket的输出流对象，把数据写出
		String data = "现在空气好点了，你们不那么热了，其实我还是挺好！";
		OutputStream out = socket.getOutputStream();
		//第三步：  把数据写出
		out.write(data.getBytes());
		
		
		//读取服务端反馈的信息
		InputStream inputStream = socket.getInputStream();
		byte[] buf   = new byte[1024];
		int length = inputStream.read(buf);
		System.out.println("客户端接收到的数据："+ new String(buf,0,length));
		
		
		
		
		//第四步： 关闭资源
		socket.close();
	}

}
