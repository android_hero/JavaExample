package com.cherry.example.network.tcp;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Demo2例子没有出现效果
 */
public class Demo2Server {
	
	public static void main(String[] args) throws IOException {
		//第一步： 建立tcp的服务端
		ServerSocket serverSocket = new ServerSocket(9090);
		//第二步： 接收客户端的连接
		Socket socket = serverSocket.accept();
		//第三步： 获取socket输入流
		BufferedReader   socketReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
		//获取socket的输出流
		OutputStreamWriter socketWriter= new OutputStreamWriter(socket.getOutputStream());
		
		//创建键盘的输入流对象
		BufferedReader keyReader = new BufferedReader(new InputStreamReader(System.in));
		
		String line = null; 
		while((line=socketReader.readLine())!=null){
			System.out.println("服务端接收到了："+ line);
			
			//向客户端回送一句话
			line = keyReader.readLine();
			socketWriter.write(line+"\r\n");
			socketWriter.flush();
			
		}
		
		//关闭资源
		serverSocket.close();
		
		
	}
}
