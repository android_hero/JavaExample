package com.cherry.example.network.tcp;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.InetAddress;
import java.net.Socket;

public class ImageClient {

    public static void main(String[] args) throws IOException {
        Socket socket = new Socket(InetAddress.getLocalHost(), 9090);

        FileOutputStream outputStream = new FileOutputStream("F://张慧雯小姐姐.jpg");

        InputStream socketIn = socket.getInputStream();
        byte[] bytes = new byte[1024];
        int length;
        while ((length = socketIn.read(bytes)) != -1) {
            outputStream.write(bytes, 0, length);
        }

        socket.close();
    }
}
