package com.cherry.example.network.udp;

//群聊的主入口
public class ChatMain {
	
	public static void main(String[] args) {
		ChatReceiver receiver = new ChatReceiver();
		ChatSender sender = new ChatSender();
		//启动线程
		receiver.start();
		sender.start();
		
	}

}
