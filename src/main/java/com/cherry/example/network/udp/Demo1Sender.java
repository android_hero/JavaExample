package com.cherry.example.network.udp;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

/*
网络编程也称作为socket(插座)通讯,  通讯的两端必须都要有Socket对象。

插座有分类型：三角插、 两角插、 圆孔....

java针对不同的协议也有不同的Socket.
	

UDP协议对应的Socket-------> DatagramSocket

TCP协议对应的Socket------> Socket 、 ServerSocket 
	

UDP协议的特点: 
	1. 将数据封装为数据包，不需要建立连接。
	2.每个数据包大小限制在64K中
	3.因为无连接，所以不可靠(数据包会丢失)
	4.因为不需要建立连接，所以速度快
	5.udp协议是不分客户端与服务端的，只分发送端与接收端。

在现实开发中哪些软件使用了udp协议通讯：飞秋、 红蜘蛛、 所有游戏、   视频会议 


udp通讯涉及到类：
	DatagramSocket	socket类。
	DatagramPacket  数据包类
		DatagramPacket(byte[] buf, int length, InetAddress address, int port) 
			buf：  要发送的数据。
			length：  数据的大小。 以字节为单位。
			address : 目标ip地址。
			port: 端口号。
udp通讯很像码头运输货物一样。

发送端发送的步骤：
	1. 建立udp的服务。使用无参的构造方法。
	2. 准备数据，把数据封装到数据包中。 
	3. 调用udp的服务发送数据包。
	4. 关闭资源.
*/


//发送端
public class Demo1Sender {
	
	public static void main(String[] args) throws IOException {
		//第一步 ： 建立Socket服务。(码头)
		DatagramSocket datagramSocket = new DatagramSocket();
		//第二步：准备数据，把数据封装到数据包中。
		String data = "这个是我第一个udp的例子";
		byte[] buf = data.getBytes();
		//把数据封装到了数据包
		DatagramPacket packet = new DatagramPacket(buf, buf.length,  InetAddress.getByName("对方的ip地址"),9090);
		//第三步： 调用udp的服务，发送数据包。
		datagramSocket.send(packet);
		//第四步：关闭资源---释放端口。
		datagramSocket.close();
	}

}
