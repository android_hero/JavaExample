package com.cherry.example.network.udp;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;

//接收端
public class Demo1Receiver {
	
	public static void main(String[] args) throws IOException {
		//第一步： 建立udp的接收端,并且要监听一个端口号。
		DatagramSocket socket = new DatagramSocket(9090);
		//第二步： 准备一个空的数据包，用于接收数据
		byte[] buf = new byte[1024];
		DatagramPacket packet = new DatagramPacket(buf, buf.length);
		//第三步： 调用udp的服务接收数据
		socket.receive(packet);  //阻塞型方法,没有接收到数据包之前会一直等待下去。
		System.out.println("接收端到的数据： "+ new String(buf,0,packet.getLength())); //getLength() 获取本次接收到的字节个数。
		
		//第四部： 关闭资源
		socket.close();
		
		
		
		
	}
}
